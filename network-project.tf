resource "google_compute_network" "vpc-testing" {
    name = "vpc-testing"
		project = local.name_id
    auto_create_subnetworks = false # This denies creating a default subnet

    depends_on = [google_project.testing-project]
}
