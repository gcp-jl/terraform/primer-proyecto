variable "service" {
    description = "List to API enable in this project"
    type = list
    default = [
        "bigquery.googleapis.com",
        "bigquerystorage.googleapis.com",
        "cloudapis.googleapis.com",
        #"cloudbuild.googleapis.com",
        "clouddebugger.googleapis.com",
        "cloudfunctions.googleapis.com",
        "cloudtrace.googleapis.com",
        #"compute.googleapis.com",
        #"container.googleapis.com",
        #"containerregistry.googleapis.com",
        "datastore.googleapis.com",
        "firebaserules.googleapis.com",
        "firestore.googleapis.com",
        #"healthcare.googleapis.com",
        "iam.googleapis.com",
        "iamcredentials.googleapis.com",
        "logging.googleapis.com",
        "monitoring.googleapis.com",
        "oslogin.googleapis.com",
        "pubsub.googleapis.com",
        #"run.googleapis.com",
        "servicemanagement.googleapis.com",
        "serviceusage.googleapis.com",
        #"source.googleapis.com",
        "sql-component.googleapis.com",
        "stackdriver.googleapis.com",
        "storage-api.googleapis.com",
        "storage-component.googleapis.com"
    ]
}