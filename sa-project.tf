resource "google_service_account" "sa-control-testing" {
	project = local.name_id    ## In this case is necessary
	account_id = "upcat-${random_integer.rand-project.result}"
	
	display_name = "Uno para controlarlos a todos"

	depends_on = [google_project.testing-project]
}

resource "google_project_iam_member" "upcat" {
	project =	local.name_id 
	role		= "roles/owner"
	member	= "serviceAccount:${google_service_account.sa-control-testing.email}"
}


## Enabled access to bucket where terrafom state is saved
resource "google_storage_bucket_iam_member" "sa-bucket-tf" {
	count = length(var.roles_bucket)
	bucket = "st-terraform-test"
	role = element(var.roles_bucket,count.index)
	member	= "serviceAccount:${google_service_account.sa-control-testing.email}"
}

## Generating json-key
resource "google_service_account_key" "upcat-key" {
	service_account_id = google_service_account.sa-control-testing.name
}

## Json-key Saved in local directory
resource "local_file" "upcat-json" {
	content = base64decode(google_service_account_key.upcat-key.private_key)
	filename = "upcat.json"
}
