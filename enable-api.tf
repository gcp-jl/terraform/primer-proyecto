resource "google_project_service" "testing-apis" {
    count = length(var.service)
    project = local.name_id
    service = var.service[count.index]

    depends_on = [google_project.testing-project]
}