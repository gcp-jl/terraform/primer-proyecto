variable "roles_bucket" {
    description = "Role list to enable r/w"
    type = list
    default = [
		"roles/storage.legacyBucketReader",
		"roles/storage.objectAdmin",
    ]
}