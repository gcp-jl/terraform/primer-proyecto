provider "google" {

}

resource "random_integer" "rand-project"{
	min = 100000
	max = 200000
}

locals {
	name_id = "testing-${random_integer.rand-project.result}"
}

resource "google_project" "testing-project"{
	name				= "testing"
	project_id	= local.name_id
	billing_account     = var.billing_id # Here is necessary if want set your network. Please read README
}
