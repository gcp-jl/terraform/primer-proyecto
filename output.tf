output "project" {
    value = local.name_id
}

output "sa-email" {
    value = google_service_account.sa-control-testing.email
}

output "network" {
	value = google_compute_network.vpc-testing.id
}
